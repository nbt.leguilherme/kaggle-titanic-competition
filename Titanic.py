from codecs import ignore_errors
import pandas as pd 
import matplotlib.pyplot as plt
import numpy as np
import os, copy
from sklearn.pipeline import make_pipeline  
from sklearn.preprocessing import StandardScaler
from sklearn.ensemble import RandomForestClassifier
from sklearn.experimental import enable_iterative_imputer
from sklearn.impute import IterativeImputer
from sklearn.model_selection import cross_val_score

class Titanic:
    def __init__(self, train_file, test_file) -> None:
        self.train = pd.read_csv(train_file)
        self.test = pd.read_csv(test_file)

    def dataAnalysis(self, directory):
        print("#################################")
        print("#         DATA ANALYSIS         #")
        print("#################################")
        print( "## General information")
        print("=================================")
        nTrain = self.train.shape[0]
        nTest = self.test.shape[0]
        nPassenger = nTrain + nTest
        print("      There is %d passengers on the ship, with %d (%.1f %%) in the learning dataset and %d (%.1f %%) in the test." % 
              (nPassenger, nTrain, nTrain/nPassenger*100, nTest, nTest/nPassenger*100))
        nSurvived = self.train.Survived.eq(1).sum()
        print("      In the learning dataset, number of survived is %d (%.1f %%)." % (nSurvived, nSurvived/nTrain*100))        
        
        print("=================================")
        print("Histograms")
        print("=================================")
        train_summarize = self.train[['PassengerId', 'Sex']].groupby('Sex').count()
        test_summarize = self.test[['PassengerId', 'Sex']].groupby('Sex').count()
        summarize = train_summarize.merge(test_summarize, left_index = True, right_index = True, suffixes = ['_Train', '_Test'])
        summarize.columns = summarize.columns.str.replace('PassengerId_', '')
        summarize = summarize.reset_index()
        summarize.plot(x = 'Sex', kind = 'bar', title = 'Distribution of gender', 
                       xlabel = 'Gender', ylabel = 'Nb of passengers', 
                       figsize = (8,6), rot = 0)
        plt.savefig(os.path.join(directory, "histogram_sex.png"))
        plt.close()

        train_summarize = self.train[['PassengerId', 'Pclass']].groupby('Pclass').count()
        test_summarize = self.test[['PassengerId', 'Pclass']].groupby('Pclass').count()
        summarize = test_summarize.merge(test_summarize, left_index = True, right_index = True, suffixes = ['_Train', '_Test'])
        summarize.columns = summarize.columns.str.replace('PassengerId_', '')
        summarize = summarize.reset_index()
        summarize.plot(x = 'Pclass', kind = 'bar', title = 'Distribution of passenger classes', 
                       xlabel = 'Pclass', ylabel = 'Nb of passengers', 
                       figsize = (8,6), rot = 0)
        plt.savefig(os.path.join(directory, "histogram_pclass.png"))
        plt.close()

        fig, ax = plt.subplots(figsize = (8,6))
        ax.hist([self.train.Age.to_numpy(), self.test.Age.to_numpy()], label = ['Train', 'Test'], density=True)
        plt.legend()
        ax.set_xlabel('Passenger age')
        ax.set_ylabel('Frequency (%)')
        ax.set_title('Distribution of passenger age')
        plt.savefig(os.path.join(directory, "histogram_age.png"))

        train_summarize = self.train[['PassengerId', 'SibSp']].groupby('SibSp').count()
        test_summarize = self.test[['PassengerId', 'SibSp']].groupby('SibSp').count()
        summarize = test_summarize.merge(test_summarize, left_index = True, right_index = True, suffixes = ['_Train', '_Test'])
        summarize.columns = summarize.columns.str.replace('PassengerId_', '')
        summarize = summarize.reset_index()
        summarize.plot(x = 'SibSp', kind = 'bar', title = 'Distribution of nb of siblings/spouses', 
                       xlabel = 'SibSp', ylabel = 'Nb of passengers',
                       figsize = (8,6), rot = 0)
        plt.savefig(os.path.join(directory, "histogram_sibsp.png"))
        plt.close()

        train_summarize = self.train[['PassengerId', 'Parch']].groupby('Parch').count()
        test_summarize = self.test[['PassengerId', 'Parch']].groupby('Parch').count()
        summarize = test_summarize.merge(test_summarize, left_index = True, right_index = True, suffixes = ['_Train', '_Test'])
        summarize.columns = summarize.columns.str.replace('PassengerId_', '')
        summarize = summarize.reset_index()
        summarize.plot(x = 'Parch', kind = 'bar', title = 'Distribution of nb of parents/children', 
                       xlabel = 'Parch', ylabel = 'Nb of passengers',
                       figsize = (8,6), rot = 0)
        plt.savefig(os.path.join(directory, "histogram_parch.png"))
        plt.close()

        fig, ax = plt.subplots(figsize = (8,6))
        ax.hist([self.train.Fare.to_numpy(), self.test.Fare.to_numpy()], label = ['Train', 'Test'], density=True)
        plt.legend()
        ax.set_xlabel('Fare')
        ax.set_ylabel('Frequency (%)')
        ax.set_title('Distribution of fare')
        plt.savefig(os.path.join(directory, "histogram_fare.png"))
        plt.close()

    def _factorizeString(self):
        variable_list = self.train.dtypes[self.train.dtypes.eq('object')].index
        for variable in variable_list:
            self.train['Numerical%s' % variable] = self.train[variable].factorize()[0]
            self.test['Numerical%s' % variable] = self.test[variable].factorize()[0]

    def _imputeMissingValue(self):
        imp = IterativeImputer(max_iter=100, random_state=10, sample_posterior=True, min_value=0)
        train_full = self.train.loc[~self.train.isnull().any(axis=1), 
                                     ['Survived', 'Pclass', 'NumericalSex', 'Age', 'SibSp', 'Parch', 
                                      'NumericalTicket', 'Fare', 'NumericalCabin', 'NumericalEmbarked']]
        train_imput = self.train.loc[self.train.isnull().any(axis=1), 
                                    ['Survived', 'Pclass', 'NumericalSex', 'Age', 'SibSp', 'Parch', 
                                     'NumericalTicket', 'Fare', 'NumericalCabin', 'NumericalEmbarked']]
        imp.fit(train_full)
        train_imput_predict = imp.transform(train_imput)
        for i in range(len(train_imput.columns)):
            var = train_imput.columns[i]
            train_imput[var] = train_imput_predict[:,i]
        #train_imput['Age'] = train_imput_predict[:,np.where(train_imput.columns == "Age")[0][0]]
        self.train_new = pd.concat([train_imput, train_imput], axis=0, ignore_index=True)

        test_full = self.test.loc[~self.test.isnull().any(axis=1), 
                                  ['PassengerId', 'Pclass', 'NumericalSex', 'Age', 'SibSp', 'Parch', 
                                   'NumericalTicket', 'Fare', 'NumericalCabin', 'NumericalEmbarked']]
        test_imput = self.test.loc[self.test.isnull().any(axis=1), 
                                   ['PassengerId', 'Pclass', 'NumericalSex', 'Age', 'SibSp', 'Parch', 
                                    'NumericalTicket', 'Fare', 'NumericalCabin', 'NumericalEmbarked']]
        imp.fit(test_full[['Pclass', 'NumericalSex', 'Age', 'SibSp', 'Parch', 
                           'NumericalTicket', 'Fare', 'NumericalCabin', 'NumericalEmbarked']])
        test_imput_predict = imp.transform(test_imput[['Pclass', 'NumericalSex', 'Age', 'SibSp', 'Parch', 
                                                       'NumericalTicket', 'Fare', 'NumericalCabin', 'NumericalEmbarked']])
        for i in range(1,len(test_imput.columns)):
            var = test_imput.columns[i]
            test_imput[var] = test_imput_predict[:,i-1]
        #test_imput['Age'] = test_imput_predict[:,np.where(test_imput.columns == 'Age')[0][0]]
        self.test_new = pd.concat([test_full, test_imput], axis=0, ignore_index=True).sort_values(by=['PassengerId'])
        
    def _buildModel(self):
        self.model = make_pipeline(StandardScaler(), RandomForestClassifier())
        self.model.fit(self.train_new[['Pclass', 'NumericalSex', 'Age', 'SibSp', 'Parch', 
                                       'NumericalTicket', 'Fare', 'NumericalCabin', 'NumericalEmbarked']], 
                        self.train_new['Survived'])

    def _evaluateModel(self):
        scores = cross_val_score(
            self.model, 
            self.train_new[['Pclass', 'NumericalSex', 'Age', 'SibSp', 'Parch', 
                            'NumericalTicket', 'Fare', 'NumericalCabin', 'NumericalEmbarked']], 
            self.train_new.Survived, 
            cv = 100, 
            scoring='accuracy'
        )
        print(np.median(scores), np.mean(scores))
    
    def _predict(self,directory, filename):
        self.result = self.test_new[['PassengerId']]
        self.result['Survived'] = self.model.predict(self.test_new[['Pclass', 'NumericalSex', 'Age', 'SibSp', 'Parch', 
                                                                    'NumericalTicket', 'Fare', 'NumericalCabin', 'NumericalEmbarked']])
        self.result.to_csv(os.path.join(directory, filename), index = False)

    def launch(self, directory, filename):
        self._factorizeString()
        self._imputeMissingValue()
        self._buildModel()
        self._evaluateModel()
        self._predict(directory, filename)

